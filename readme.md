#matrix_bench

A simple matrix multiplication benchmark by [Alexandre
Medeiros](https://github.com/alemedeiros).

##Compiling
The files comes with a makefile, which might help the compilation, just use
`make bench` to compile the benchmark for the current system, if you want to
cross-compile the bench mark, you must define the toolchain binaries prefix on
the `CROSS_COMPILE` shell variable.

Example: Cross-compiling for arm-none-linux-gnueabi, with the toolchain on
`~/toolchain/`

    $ export CROSS_COMPILE=~/toolchain/arm-none-linux-gnueabi-4.4.1/bin/arm-none-linux-gnueabi-
    $ make cross

NOTE: When cross-compiling, you might want to change the `CROSS_FLAGS` on the
makefile.

##Fine-tuning the benchmark
You can change the time the benchmark is executed, the number of threads it uses
and the matrix size. All this can be done through compilation flags.

* time executed: add or change the `-DTIME_LIM=<number>` flag on the compilation
  line on the makefile, where `number` is the desired execution time for the
  benchmark.
* number of threads: add or change the `-DTHREADS_N=<number>` flag on the
  compilation line on the makefile, where `number` is the number of desired
  threads for the benchmark.
* matrix size: add or change the `-DSZ=<number>` flag on the compilation line on
  the makefile, where `number` is the size of the matrix you want the benchmark
  to use.

NOTE: It is recomended to keep the binary files in the following pattern
`bench-<size>-<threads>-<time>`.
