/** matrix.h -- a simple matrix library
 *    by alemedeiros <alexandre.medeiros@students.ic.unicamp.br>
 *
 * Designed to be used as a small benchmark, only works with square matrices.
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <stdlib.h>

/* square matrix */
struct mat
{
  int* m;
  size_t sz;
};

struct mat* matrix_read(size_t n);
void matrix_print(struct mat* a, FILE* stream);
struct mat* matrix_alloc(size_t n);
void matrix_free(struct mat* a);
struct mat* matrix_transpose(struct mat* a);
struct mat* matrix_sum(struct mat* a, struct mat* b);
/* cache-aware multiplication. */
struct mat* matrix_multiply(struct mat* a, struct mat* b);
/* naïve multiplication. */
struct mat* matrix_multiply2(struct mat* a, struct mat* b);

#endif /* MATRIX_H */
