/** bench.c -- matrix_bench
 *    by alemedeiros <alexandre.medeiros@students.ic.unicamp.br>
 *
 * Small matrix multiplication benchmark.
 */

#include "matrix.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* matrix size defaults to 100. */
#ifndef SZ
#define SZ 100
#endif /* SZ */

/* number of calculating threads defaults to 2. */
#ifndef THREADS_N
#define THREADS_N 2
#endif /* THREADS_N */

/* benchmark time limit (seconds) defaults to half minute. */
#ifndef TIME_LIM
#define TIME_LIM 30
#endif /* TIME_LIM */

/*** global variables ***/
/* input matrices. */
struct mat* a;
struct mat* b;

void* multiply(void* arg)
{
    unsigned* counter = arg;
    struct mat* res;

    while (1) {
        /* multiply */
        res = matrix_multiply(a, b);
        *counter += 1;
        matrix_free(res);
    }

    return NULL;
}

int main(int argc, char** argv)
{
    pthread_t threads[THREADS_N];
    unsigned count[THREADS_N];
    unsigned mult_total = 0;
    unsigned i, j;
    time_t init, current;

    /* read matrices. */
    a = matrix_read(SZ);
    b = matrix_read(SZ);

    /* create multiplications threads. */
    for (i = 0; i < THREADS_N; i++) {
        count[i] = 0;
        pthread_create(&threads[i], NULL, multiply, (void*) &count[i]);
    }

    /* polling */
    init = time(NULL);
    while (1) {
        current = time(NULL);

        if (current - init > TIME_LIM)
            break;
    }

    exit(EXIT_SUCCESS);
}
