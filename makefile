FLAGS := -O2 -pthread
CROSS_FLAGS := -march=armv7-a -static
SRC := bench.c matrix.c

all: $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=1 -DSZ=32   -o bench-32-1-45   $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=2 -DSZ=32   -o bench-32-2-45   $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=4 -DSZ=32   -o bench-32-4-45   $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=1 -DSZ=2048 -o bench-2048-1-45 $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=2 -DSZ=2048 -o bench-2048-2-45 $(SRC)
	gcc $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=4 -DSZ=2048 -o bench-2048-4-45 $(SRC)

cross: $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=1 -DSZ=32   -o bench-32-1-45   $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=2 -DSZ=32   -o bench-32-2-45   $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=4 -DSZ=32   -o bench-32-4-45   $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=1 -DSZ=2048 -o bench-2048-1-45 $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=2 -DSZ=2048 -o bench-2048-2-45 $(SRC)
	$(CROSS_COMPILE)gcc $(CROSS_FLAGS) $(FLAGS) -DTIME_LIM=45 -DTHREADS_N=4 -DSZ=2048 -o bench-2048-4-45 $(SRC)

.PHONY: all cross
