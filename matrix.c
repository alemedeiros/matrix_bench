/** matrix.c -- a simple matrix library
 *    by alemedeiros <alexandre.medeiros@students.ic.unicamp.br>
 *
 * Designed to be used as a small benchmark, only works with square matrices.
 */

#include "matrix.h"
#include <stdio.h>

/* macro to get matrix's i,j element. */
#define _M(m,i,j,sz) (*((m)+(i*sz)+j))

/** matrix_read
 * read int matrix from stream.
 */
struct mat* matrix_read(size_t n)
{
    int i, j;
    struct mat* a;

    a = matrix_alloc(n);

    /*
    for (i = 0; i < n; ++i)
        for (j = 0; j < n; ++j)
            fscanf(stream, "%i", &(_M(a->m, i, j, a->sz)));
    */

    return a;
}

/** matrix_print
 * print int matrix to stream.
 */
void matrix_print(struct mat* a, FILE* stream)
{
    int i, j;

    for (i = 0; i < a->sz; ++i) {
        for (j = 0; j < a->sz; ++j)
            printf("%i ", _M(a->m, i, j, a->sz));

        putchar('\n');
    }

    return;
}

/** matrix_alloc
 * alloc matrix of size n.
 */
struct mat* matrix_alloc(size_t n)
{
    struct mat* a;

    a = malloc(sizeof(struct mat));
    a->m = malloc(sizeof(int) * n * n);
    a->sz = n;

    return a;
}

/** matrix_free
 * free matrix a.
 */
void matrix_free(struct mat* a)
{
    free(a->m);
    free(a);

    return;
}

/** matrix_transpose
 * transpose matrix a.
 * return a^t
 */
struct mat* matrix_transpose(struct mat* a)
{
    int i, j;
    struct mat* b;

    b = matrix_alloc(a->sz);

    for (i = 0; i < a->sz; ++i)
        for (j = 0; j < a->sz; ++j)
            _M(b->m, i, j, b->sz) = _M(a->m, j, i, a->sz);

    return b;
}

/** matrix_sum
 * return a + b
 */
struct mat* matrix_sum(struct mat* a, struct mat* b)
{
    int i, j;
    struct mat* c;

    if (a->sz != b->sz)
        return NULL;

    c = matrix_alloc(a->sz);

    for (i = 0; i < a->sz; ++i)
        for (j = 0; j < a->sz; ++j)
            _M(c->m, i, j, c->sz) = _M(a->m, i, j, a->sz) + _M(b->m, i, j, b->sz);

    return c;
}

/** matrix_multiply
 * cache-aware multiplication. transpose b, to reduce cache misses.
 * return a * b
 */
struct mat* matrix_multiply(struct mat* a, struct mat* b)
{
    int i, j, k;
    struct mat* t;
    struct mat* c;

    if (a->sz != b->sz)
        return NULL;

    c = matrix_alloc(a->sz);
    t = matrix_transpose(b);

    for (i = 0; i < a->sz; ++i)
        for (j = 0; j < a->sz; ++j) {
            _M(c->m, i, j, c->sz) = 0;
            for (k = 0; k < a->sz; ++k)
                _M(c->m, i, j, c->sz) += _M(a->m, i, k, a->sz) + _M(t->m, j, k, t->sz);
        }

    matrix_free(t);

    return c;
}

/** matrix_multiply2
 * naïve matrix multiplication.
 * return a * b
 */
struct mat* matrix_multiply2(struct mat* a, struct mat* b)
{
    int i, j, k;
    struct mat* c;

    if (a->sz != b->sz)
        return NULL;

    c = matrix_alloc(a->sz);

    for (i = 0; i < a->sz; ++i)
        for (j = 0; j < a->sz; ++j) {
            _M(c->m, i, j, c->sz) = 0;
            for (k = 0; k < a->sz; ++k)
                _M(c->m, i, j, c->sz) += _M(a->m, i, k, a->sz) + _M(b->m, k, j, b->sz);
        }

    return c;
}
